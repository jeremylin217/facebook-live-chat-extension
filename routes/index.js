var express = require('express')
var router = express.Router()
var debug = require('debug')('app')
var compose = require('compose-middleware').compose
var fetch = require('isomorphic-fetch')

const FB_GRAPH_API = 'https://graph.facebook.com/v3.0'
const FB_APP_ID = '225383718195473'
const FB_APP_SECRET = '13d1c9d5a8b624bf8808dd164b9170eb'

var getFetchOptions = (method, rawbody, token) => {
  var options = {}
  options.method = method

  if (method === 'POST') {
    options.headers = {}
    options.headers['Content-Type'] = 'application/json'
  }

  if (token) options.headers['Authorization'] = 'Bearer ' + token
  if (rawbody) options.body = JSON.stringify(rawbody)

  return options
}

var debugToken = (body, done) => {
  const url = FB_GRAPH_API + '/debug_token?' +
  'input_token=' + body.tempToken + '&access_token=' + FB_APP_ID + '|' + FB_APP_SECRET
  fetch(url, getFetchOptions('GET', null))
  .then(response => response.json())
  .then(obj => {
    debug({debugToken: JSON.stringify(obj)})
    if (obj.error) return done({error: 'FacebookTokenDebugError', object: obj.error}, null)
    else return done(null, body)
  })
}

var accessToken = (body, done) => {
  const url = FB_GRAPH_API + '/oauth/access_token?' +
  'client_id=' + FB_APP_ID + '&client_secret=' + FB_APP_SECRET +
  '&grant_type=fb_exchange_token&fb_exchange_token=' + body.tempToken
  fetch(url, getFetchOptions('GET', null))
  .then(response => response.json())
  .then(obj => {
    if (obj.error) {
      debug({accessToken: JSON.stringify(obj)})
      return done({Error: 'FacebookTokenError', Message: 'Failed to login with Facebook. Please contact us.'}, null)
    }
    body.accessToken = obj.access_token
    body.tokenType = obj.token_type
    body.expiresIn = obj.expires_in
    return done(null, body)
  })
}

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' })
})

router.get('/app', function (req, res, next) {
  res.render('app', { title: 'Express' })
})

router.post('/facebook/login', function (req, res, next) {
  debug({body: req.body})
  if (!req.body.LoginResponse) {
    debug({Error: 'MissingFieldError', Message: 'Require field name: `LoginResponse`.'})
    return res.status(400).send({Error: 'MissingFieldError', Message: 'Require field name: `LoginResponse`.'})
  }
  if (!res.body) res.body = {}
  if (req.body.LoginResponse && req.body.LoginResponse.status === 'connected' && req.body.LoginResponse.authResponse) {
    res.body.tempToken = req.body.LoginResponse.authResponse.accessToken
    res.body.facebookId = req.body.LoginResponse.authResponse.userID
  }
  debugToken(res.body, (err, data) => {
    debug({err, data})
    accessToken(data, (er, da) => {
      return res.status(200).send({FacebookId: res.body.facebookId})
    })
  })
})

router.get('/messenger', function (req, res, next) {
  debug({query: req.query})
  debug({headers: req.headers})
  if (req.query['hub.mode'] === 'subscribe' &&
    req.query['hub.verify_token'] === 'uyo3thuijrrhuipJHOrCq3V78Vrhi') {
    debug('Validating webhook')
    return res.status(200).send(req.query['hub.challenge'])
  } else {
    debug('Failed validation. Make sure the validation tokens match.')
    return res.sendStatus(403)
  }
})

router.post('/messenger', function (req, res, next) {
  var data = req.body
  debug({body: JSON.stringify(req.body)})
  if (data.object === 'page') {
    data.entry.forEach((entry) => {
      var pageId = entry.id
      var timestamp = entry.time
      var pageInfo = {
        pageId: ''
      }
      entry.messaging.forEach((event) => {
        if (event.message) {
          debug({message: 'Text message received.', event: event})
        } else if (event.postback) {
          receivedPostback(pageInfo, event)
        } else {
          debug({message: 'Webhook received unknown event.', event: event})
        }
      })
    })
    return res.status(200).send()
  }
})

module.exports = router
