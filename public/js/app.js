window.fbAsyncInit = function () {
  FB.init({
    appId: '225383718195473',
    version: 'v3.0',
    cookie: true,
    xfbml: false
  })
}

$(function () {
  $('#fblogin').on('click', () => {
    FB.login(response => (response.status === 'connected')
      ? getVideo(response.authResponse)
      : null
      , {scope: 'user_videos'})
  })
})

function getVideo ({ accessToken, userID }) {
  var url = `https://graph.facebook.com/${userID}/live_videos?redirect=false&access_token=${accessToken}`
  $.get(url, response => {
    if (response.data && response.data[0]) {
      $('#root').html(response.data[0].embed_html)
    }
  })
}

// class App extends React.Component {
//   render () {
//     const isLogin = false
//     if (isLogin) {
//       return (
//         <div>
//           <iframe src='https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fdidigh%2Fvideos%2F10156406311488308%2F&width=400' width='400' height='400' scrolling='no' frameBorder='0' allowTransparency='true' allowFullScreen='true' />
//         </div>
//       )
//     } else {
//       return (
//         <Login />
//       )
//     }
//   }
// }

// class Login extends React.Component {
//   handleLogin () {
//     FB.login(response => (response.status === 'connected')
//       ? console.log('response: ', response)
//       : null
//       , {scope: 'user_videos'})
//   }

//   render () {
//     return (
//       <div className='container text-center'>
//         <button className='btn btn-primary btn-block' onClick={this.handleLogin}>
//           Facebook Login
//         </button>
//       </div>
//     )
//   }
// }

// ReactDOM.render(
//   <App />,
//   document.getElementById('root')
// )
